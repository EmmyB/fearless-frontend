import Nav from './Nav.jsx'
import AttendeesList from './AttendeesList.jsx'
import LocationForm from './LocationForm.jsx'
import ConferenceForm from './ConferenceForm.jsx'



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className='container'>
        <LocationForm />
        <ConferenceForm />
        <AttendeesList attendees={props.attendees} />
      </div>
    </>
  );
}

export default App